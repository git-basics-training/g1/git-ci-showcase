using NUnit.Framework;

namespace showcase.test
{
    public class CalcEngineTests
    {
        private CalcEngine _engine;
        [SetUp]
        public void Setup()
        {
            _engine = new CalcEngine();
        }

        [Test]
        public void OnePlusOne_should_return_two()
        {
            var x = 1;
            var y = 1;
            var expected = 2;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
        [Test]
        public void OnePlusTwo_should_return_three()
        {
            var x = 1;
            var y = 2;
            var expected = 3;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
        [Test]
        public void TwoPlusOne_should_return_three()
        {
            var x = 2;
            var y = 1;
            var expected = 3;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
        [Test]
        public void sixPlusSix_should_return_tvelve()
        {
            var x = 6;
            var y = 6;
            var expected = 12;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
        [Test]
        public void sixPlusone_should_return_tvelve()
        {
            var x = 6;
            var y = 6;
            var expected = 12;

            var result = _engine.Add(x, y);

            Assert.AreEqual(expected, result);
        }
    }
}